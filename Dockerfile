FROM openjdk:12
ADD target/treinamento-docker-spring.jar treinamento-docker-spring.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "treinamento-docker-spring.jar"]