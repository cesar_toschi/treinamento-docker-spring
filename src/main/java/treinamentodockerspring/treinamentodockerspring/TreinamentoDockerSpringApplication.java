package treinamentodockerspring.treinamentodockerspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TreinamentoDockerSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(TreinamentoDockerSpringApplication.class, args);
	}

}
